package testbankbazaar;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bbpages.BBHomePage;

import wdMethods1.ProjectMethodsbb;
import wdMethods1.SeMethodsbb;

public class BankBazaar extends ProjectMethodsbb {
	
	
	@BeforeClass
	public void setData() {
		testCaseName = "BankBazaar";
		testCaseDescription ="Findings Scheme";
		category = "SIT";
		author= "Manju";
		
	}
	
	@Test
	public  void findInvestmentOptions()   {
		new BBHomePage()	
		.Investmentmouseover()
		.MutualFundmouseover()
		.searchMutualFundsClick()
		.ageSlider()
		.monthAndYearSelection()
		.daySelection()
		.verifyDOB()
		.clickContinue1()
		.netAmountSlider()
		.clickContinue2()
		.bankSelection()
		.enterMyFirstName()
		.viewMutualFundsclick()
		.printSchemeName();		
		
	}
	


}
