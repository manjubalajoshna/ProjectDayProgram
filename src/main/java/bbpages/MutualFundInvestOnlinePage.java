package bbpages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods1.SeMethodsbb;

public class MutualFundInvestOnlinePage extends SeMethodsbb {
	
public MutualFundInvestOnlinePage searchMutualFundsClick() {
	
	WebElement searchmutualfunds = locateElement("xpath", "//a[text()='Search for Mutual Funds']");
	searchmutualfunds.click();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}
	
public MutualFundInvestOnlinePage ageSlider() {
	
	WebElement AgeSlider1 = locateElement("xpath", "//div[@class='rangeslider__handle-label']");
	//WebElement AgeSlider2 = locateElement("xpath", "//div[text()='27']");
	//AgeSlider2.click();
	Actions builder = new Actions(driver);
	builder.dragAndDropBy(AgeSlider1, 100, 0).build().perform();
	
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}
	
public MutualFundInvestOnlinePage monthAndYearSelection() {
	

	WebElement MonthandYear = locateElement("xpath", "(//table[@class='Calendar_curvedTable_PGAnV']//td)[1]");
	MonthandYear.click();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}

public MutualFundInvestOnlinePage daySelection() {
	
	WebElement day = locateElement("xpath", "//div[@class='react-datepicker__week']//div[7]");
	day.click();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}

public MutualFundInvestOnlinePage verifyDOB() {
	
	WebElement day = locateElement("class", "Calendar_highlight_xftqk");
	
	verifyExactText(day, "6 Sep 1990");	
	System.out.println("day" + day.getText());
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}
	
public MutualFundInvestOnlinePage clickContinue1() {
	
	WebElement Continue1 = locateElement("xpath", "//a[text()='Continue']");
	Continue1.click();	
	return this;
}

public MutualFundInvestOnlinePage netAmountSlider() {
	
	WebElement netAmountSlider1 = locateElement("xpath", "//div[@style='left: 56px;']");
	//WebElement netAmountSlider2 = locateElement("xpath", "//div[@style='left: 257px;']");
	Actions builder = new Actions(driver);
	builder.dragAndDropBy(netAmountSlider1, 100, 0).build().perform();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}

public MutualFundInvestOnlinePage clickContinue2() {
	
	WebElement Continue2 = locateElement("xpath", "//a[text()='Continue']");
	Continue2.click();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}

public MutualFundInvestOnlinePage bankSelection() {
	
	WebElement bankSelection = locateElement("class", "PrimaryAccount_iconHdfcBank_3lPx8");
	bankSelection.click();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return this;
}

public MutualFundInvestOnlinePage enterMyFirstName() {
	
	WebElement myFirstName = locateElement("name", "firstName");
	type(myFirstName, "Jositha");
	return this;
}

public MutualFundsSchemePage viewMutualFundsclick()  {
	
	WebElement viewMutualFunds = locateElement("xpath", "//a[text()='View Mutual Funds']");
	viewMutualFunds.click();
	try {
		Thread.sleep(7000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return new MutualFundsSchemePage();
}

















}

